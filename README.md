# API to auto detect multiple user access with laravel

Login, Register, Logout with token as session in Header.
Validation input when register & login. Remove session when logout.
Authentication use email & password.
Authorization use token.

## Required
1. PHP > 7.2
2. Composer
3. MySql / PostgreSql

## Optional
- Postman / insomnia tool to testing api

## Run
Before run must be clone or download this repository.
1. Copy Paste .env.example to .env . Or from console: run `cp -r .env.example .env`
2. Run `php artisan key:generate` to create your secret app key
3. Setup your databasehost & databasename from .env file. Change the DB_CONNECTION, DB_DATABASE, DB_USERNAME, DB_PASSWORD
```
DB_CONNECTION=
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
4. Run `composer install` to install dependencies (vendor folder)
5. Run `php artisan migrate` to create table to database
6. Run `php artisan db:seed --class=DatabaseSeederD` to create data dummy and store to database
7. Run `php artisan serve` to run app server. And now app running in `http://127.0.0.1:8000` or `http://localhost:8000`
8. Go to `http://127.0.0.1:8000/api/v1/` to access api
9. Check endpoint list to get endpoint (url)

## Endpoint
> __http://localhost:8000/api/v1__ or __http://127.0.0.1:8000/api/v1__ as __baseurl__
> __baseurl__/_endpoint_ to access full URL, example:
> __http://localhost:8000/api/v1/login__

> _Always_ _adding_ __'Accept':'application/json'__ _to_ _Header_

> After Login add __'Authorization':'Bearer <api_token>'__ to Header

### __Endpoint List__

|  Endpoint       | Method | Request (Input) from body | Role                                 |
| --------------  | ------ | ------------------------- | ------------------------------------ |
| __/login__      | GET    | * email                   | string, email, unique                |
|                 |        | * password                | string, min 8 character,             |
|                 |        |                           | 1 number, 1 special character        |
| __/logout__     | POST   | - id                      |                                      |
| __/register__   | POST   | - name                    | string, min 5 alphabet. Allow space  |
|                 |        | - email                   | string, email,                       |
|                 |        | - password                | string, min 8 character,             |
|                 |        |                           | 1 number, 1 special character        |
|                 |        | * phone                   | number, min 10 digit, max 13 digit   |
|                 |        | * attachment_url          | string, allow array string,          |
|                 |        | * type                    | option, value:'A','B','C'            |
| __/__           | GET    |                           |                                      | 
| __/attachment__ | POST   | - attach[]                | file, array. After upload will       |
|                 |        |                           | get url to pass to __attachmen_url__ |
|                 |        |                           | in endpoint __/register__            |


### __Optional Endpoint__

|  Endpoint       | Method | Request (Input) from body | Role                        |
| --------------  | ------ | ------------------------- | --------------------------- |
| __/recaptcha__  | POST   | secret (required)         | Google Recaptcha Secret Key |
|                 |        | response (required)       | Google Recaptcha Site Key   |
|                 |        | remoteip (optional)       | IP                          |

### __Response List__

| Http Code | Http Status                   |
| :-------: | ----------------------------: |
| 200       | success (get data)            |
| 201       | success (create)              |
| 401       | error (validation, header)    |
| 404       | error (not found)             |
| 500       | error (internal server error) |

