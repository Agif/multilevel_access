<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    </head>
    <body>
        <form action="{{ route('recaptcha') }}" method="post">
        
            {{ csrf_field()  }}
            <input type="hidden" name="recaptcha" id="recaptcha">
            <div class="form-group float-right btn-feedback-block">
                <button @click.prevent="leaveFeedback()" type="submit" class="btn btn-outline-primary btn-lg">Send</button>
            </div>
        </form>
        
        <script src="https://www.google.com/recaptcha/api.js?render={{ env('GOOGLE_RECAPTCHA_KEY') }}"></script>
        <script>
                 grecaptcha.ready(function() {
                     grecaptcha.execute('{{ env('GOOGLE_RECAPTCHA_KEY') }}', {action: 'contact'}).then(function(token) {
                        if (token) {
                          document.getElementById('recaptcha').value = token;
                        }
                     });
                 });
        </script>
    </body>
</html>
