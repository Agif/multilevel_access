<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function(){
    //Route::get('captcha', 'ReCaptchaController@captcha')->name('captcha'); // uncoment to test as frontend sample
    Route::post('recaptcha', 'ReCaptchaController@SendCaptcha')->name('recaptcha');
    
    Route::post('attachment', 'AttachmentController@attach')->name('attach');
    
    Route::post('register', 'Api\AuthController@register')->name('register');
    Route::post('login', 'Api\AuthController@login')->name('login');
 
    Route::group(['middleware' => 'auth:slim'], function(){
        Route::get('/', 'Api\DashboardController@index')->name('dashboard');
    
        Route::post('logout', 'Api\AuthController@logout')->name('logout');
    });
});
