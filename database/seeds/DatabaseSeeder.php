<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Article::class, 30)->create();
        $this->call([
            UserSeeder::class,
            VideoSeeder::class,
        ]);
    }
}
