<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                "name" => "user",
                "email" => "user@email.com",
                "password" => Hash::make("password1!"), // password1!
                "phone" => "085728274906",
                "attachment_url" => null,
                "type" => "A",
            ],
            [
                "name" => "admin",
                "email" => "admin@email.com",
                "password" => Hash::make("password1!"), // password1!
                "phone" => "08989900272",
                "attachment_url" => null,
                "type" => "B",
            ],
            [
                "name" => "root",
                "email" => "root@email.com",
                "password" => Hash::make("password1!"), // password1!
                "phone" => "081212408246",
                "attachment_url" => null,
                "type" => "C",
            ],
        ]);
    }
}
