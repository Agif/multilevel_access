<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\Response;
use Exception;

use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //AuthorizationException::class,
        //HttpException::class,
        //ModelNotFoundException::class,
        //ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        //'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $e)
    {
        if (env('APP_DEBUG')) {
          return parent::render($request, $e);
        }
        
        if ($e instanceof ModelNotFoundException) {
            
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
            
            return response()->json([
                'success' => false,
                'message' => $this->unauthenticated($request, $e)
            ], $status);
          
        }elseif ($e instanceof MethodNotAllowedHttpException) {
            
            $status = Response::HTTP_METHOD_NOT_ALLOWED;
            
            return response()->json([
                'success' => false,
                'message'=> new MethodNotAllowedHttpException([], 'HTTP_METHOD_NOT_ALLOWED', $e)
            ], $status);
        
        }elseif ($e instanceof NotFoundHttpException) {
            
            $status = Response::HTTP_NOT_FOUND;
            
            return response()->json([
                'success' => false,
                'message' => new NotFoundHttpException('HTTP_NOT_FOUND', $e)
            ], $status);
        
        }elseif ($e instanceof ModelNotFoundException) {
            
            $status = Response::HTTP_NOT_FOUND;
            
            return response()->json([
                'success' => false,
                'message' => new ModelNotFoundException('HTTP_NOT_FOUND', $e)
            ], $status);
        
        }elseif ($e instanceof AuthorizationException) {
            
            $status = Response::HTTP_FORBIDDEN;
            
            return response()->json([
                'success' => false,
                'message' => new AuthorizationException('HTTP_FORBIDDEN', $status)
            ], $status);
        
        }elseif ($e instanceof \Dotenv\Exception\ValidationException && $e->getResponse()) {
          
            $status = Response::HTTP_BAD_REQUEST;
            
            return response()->json([
                'success' => false,
                'message' => new \Dotenv\Exception\ValidationException('HTTP_BAD_REQUEST', $status, $e)
            ], $status);
        
        }else {
          
            $status = $e;
            
            return response()->json([
                'success' => false,
                'message' => new HttpException($status, 'HTTP_INTERNAL_SERVER_ERROR')
            ], $status);
        }
    }
}
