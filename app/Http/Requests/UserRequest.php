<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $segment = $request->segment(3);
        
        if($segment == 'login'){
            $rules = [
                'email'     => 'required|email|exists:users,email',
                'password'  => 'required',
            ];
        }else{
            $rules = [
                'name'       => 'required|min:5|regex:/^[a-zA-Z ]*$/',
                'email'      => 'required|email|unique:users,email',
                'password'   => 'required|regex:/^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
                'phone'      => 'required|numeric|digits_between:10,13|unique:users,phone',
                'type'       => 'required|in:A,B,C',
            ];    
        }
        
        return $rules;
    }
    
    public function messages()
    {
        return [
            'name.required'      => 'The :attribute field is required.',
            'name.min'           => 'The minimum fild name is :min characters',
            'name.regex'         => 'The name cannot contain numbers or special characters',
            'email.required'     => 'The :attribute field is required.',
            'email.email'        => 'Email format not valid',
            'email.unique'       => 'Email has been registered',
            'email.exists'       => 'User not register',
            'password.required'  => 'The :attribute field is required.',
            'password.regex'     => 'Password must be: at least 8 characters, 1 digit, 1 special character (#?! @ $% ^ & * -)',
            'phone.required'     => 'The :attribute field is required.',
            'phone.numeric'      => 'Phone must be a number',
            'phone.digits_between'=> 'The :attribute value minimal is :min digit & maximal is :max digit.',
            'phone.unique'       => 'Phone number has been registered',
            'type.required'      => 'The :attribute field is required.',
            'type.in'            => 'The :attribute must be one of the following types: :values',
        ];
    }
    
    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors()->first();
        throw new HttpResponseException(
            response()->json([
                'success' => false,
                'message' => $errors
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
