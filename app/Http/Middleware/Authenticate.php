<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Exception;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        
        try {
            // some code
            if (!$request->wantsJson()) {
                //return route('login');
                $request->headers->set('Content-Type', 'application/json');
                
                return response()->json([
                    'error' => 'Unauthorized !!!'
                ], 401);
            //}else{
            //    $request->headers->set('Accept', 'application/json');
            }
        } catch (Exception $e) {
            return response()->json(['error' => 'Need Header'], 500);
        }
        
    }
}
