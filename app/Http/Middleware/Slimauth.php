<?php

namespace App\Http\Middleware;

//use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;
use App\User;
use Auth;

class Slimauth
{
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');

        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error' => 'The api token is required'
            ], 401);

        }elseif(preg_match("/Bearer /", $token)){

            $strToken = str_replace('Bearer ','', $token);

            // check validitas request
            $user = User::where('api_token', $strToken)->first();

            // Now let's put the user in the request class so that you can grab it from there
            if(!empty($user)){

                $this->auth = $user;

            }else{

                return response()->json([
                    'error' => 'Unauthorized !!!, User not foud / the session has ended, please relogin'
                ], 401);
            }
            
            return $next($request);

        }else{
            return response()->json([
                'error' => 'Unauthorized !!!, wrong token format, please check Header. ex: Bearer <token>'
            ], 401);
        }
    }
}
