<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class AttachmentController extends Controller
{
    public function attach(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'attachment' => 'required',
            'attachment.*' => 'file|mimes:xlsx,xls,csv,jpg,jpeg,png,bmp,doc,docx,pdf,tif,tiff|max:512'
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 401);
        }
        
        $attach = array();
        
        if ($request->hasfile('attachment'))
        {
            $current_time = \Carbon\Carbon::now()->timestamp;
            
            foreach($request->file('attachment') as $file)
            {
                $path = 'attachment';
                $name = $current_time.'.'.Str::snake($file->getClientOriginalName());
                
                $file->move($path,$name);
        
                $attach[] = $path.'/'.$name;
            }    
        }
        
        return response()->json([
            'success' => true,
            'message' => $attach
        ], 201);
    }
}
