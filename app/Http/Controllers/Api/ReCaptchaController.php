<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rules\ValidRecaptcha;
use Validator;

class ReCaptchaController extends Controller
{
    public function captcha(){
        return view('recaptcha');    
    }
    
    public function SendCaptcha(Request $request)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $data = [
            'secret' => env('GOOGLE_RECAPTCHA_SECRET'),
            'response' => $request->get('recaptcha'),
            'remoteip' => $remoteip
        ];
        
        $options = [
            'http' => [
              'header' => "Content-type: application/x-www-form-urlencoded\r\n",
              'method' => 'POST',
              'content' => http_build_query($data)
            ]
        ];
        
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $resultJson = json_decode($result);

        if ($resultJson->success == false) {
            return response()->json([
                "success" => false,
                "message" => "ReCaptcha Error, not valid resquest"
            ], 401);
        } else {
            return response()->json([
                "success" => true,
                "message" => "ReCaptcha Error, not valid resquest"
            ], 200);
        }
    }
}