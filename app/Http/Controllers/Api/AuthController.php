<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use Auth;

class AuthController extends Controller
{
    public function register(UserRequest $request){
        $data = array(
            "name"           => $request->input('name'),
            "email"          => $request->input('email'),
            "password"       => Hash::make($request->input('password')),
            "phone"          => $request->input('phone'),
            "attachment_url" => $request->input('attachment_url'),
            "type"           => $request->input('type')
        );
        
        try{
            User::create($data);
            
            return response()->json([
                'success' => true,
                'message' => 'Register Success',
            ], 201);
        }catch(\Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e
            ], 500);
        }
    }
    
    public function login(UserRequest $request){
        $email = $request->input('email');
        $password = $request->input('password');

        $user = User::where('email', $email)->first();

		// Check request Password valid or no
        $checkPass = Hash::check($password, $user->password);
        
        if (!$checkPass) {
        // if ($password != $user->password) {
            return response()->json([
                'success' => false,
                'message' => ["password" => ["Wrong Password"]]
            ], 422);
        }
        
        // Login & Create Token
        if ($email == $user->email && $checkPass){

            $token = Str::random(60);
            
            try {
                
                User::where('email', $email)->update(['api_token' => $token]);
                
                return response()->json([
                    'success' => true,
                    'message' => "Success login",
                    "token" => $token,
                ], 200);
            }catch(\Exception $e){
                return response()->json([
                    'success' => false,
                    'message' => $e
                ], 500);
            }
        }
    }
    
    public function logout(Request $request){
        $id = $request->input('id');
        try{
            User::where('id', $id)->update(['api_token' => null]);
            return response()->json([
                'success' => true,
                'message' => "Success logout"
            ], 200);
        }catch(\Exception $e){
            return response()->json([
                'success' => true,
                'message' => $e
            ], 500);
        }
    }
}
