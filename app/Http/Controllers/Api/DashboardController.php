<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Article;
use App\Video;
use Auth;
use DB;

class DashboardController extends Controller
{   
    public function index() {
        $type = Auth::user();
        
        switch ($type) {
            // User
            case 'A':
                $article = DB::table('articles')->get()->take(3);
                $video = DB::table('videos')->get()->take(3);
            break;
            
            // Admin
            case 'B':
                $article = DB::table('articles')->get()->take(10);
                $video = DB::table('videos')->get()->take(10);
            break;
            
            // Root
            case 'C':
                $article = DB::table('articles')->get();
                $video = DB::table('videos')->get();    
            break;
        }
        
        return response()->json([
            'success' => true,
            'message' => 'Get videos and articles',
            'article' => [
                'couter' => $article->count(),
                'data' => $article
            ],
            'video' => [
                'couter' => $video->count(),
                'data' => $video
            ],
        ], 200);
    }
}
