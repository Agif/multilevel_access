<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'description', 'sources', 'subtitle', 'thumb', 'title'
    ];
}
